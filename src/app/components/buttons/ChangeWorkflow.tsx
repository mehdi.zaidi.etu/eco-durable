export default function ChangeWorkflow({ contract, text, state, userAdress, oldState, newState }) {
  const methods = contract.methods;

  function changeWorkflowState() {
    switch (state) {
      case 'startVotingSession':
        methods.startVotingSession().send({ from: userAdress });
        break;

      case 'endVotingSession':
        methods.endVotingSession().send({ from: userAdress });
        break;

      case 'startProposalsRegistration':
        methods.startProposalsRegistration().send({ from: userAdress });
        break;

      case 'endProposalsRegistration':
        methods.endProposalsRegistration().send({ from: userAdress });
        break;
    }
    oldState(false);
    newState(true);
  }

  return (
    <div>
      <button className='btn btn-primary mt-2' onClick={changeWorkflowState}>
        {text}
      </button>
    </div>
  );
}
