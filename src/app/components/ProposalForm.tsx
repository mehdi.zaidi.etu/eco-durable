import { useState } from 'react';

export default function ProposalForm({ contract, userAddress, setProposalCount, proposalCount }) {
  const [proposal, setProposal] = useState('');
  const methods = contract.methods;

  function handleProposalChange(event: any) {
    setProposal(event.target.value);
  }

  function handleSubmit(event: any) {
    event.preventDefault();
    methods.registerProposal(proposal).send({ from: userAddress });
    setProposal('');
    setProposalCount(proposalCount + 1);
  }

  return (
    <div>
      <h3>Ajouter une proposition</h3>
      <form onSubmit={handleSubmit}>
        <textarea className='form-control' onChange={handleProposalChange} value={proposal}></textarea>
        <button className='btn btn-primary mt-2'>Ajouter</button>
      </form>
    </div>
  );
}
