import { useState } from 'react';

export default function ProposalCard({ contract, userAddress, number, text }) {
  const methods = contract.methods;

  function vote() {
    contract.methods.vote.send({ from: userAddress });
  }

  return (
    <div className='col-md-4 card'>
      <div className='card-body'>{text}</div>
      <button onClick={vote}>Vote</button>
    </div>
  );
}
