import { useState } from 'react';

export default function VoterForm({ contract, userAdress, setVotersCount, votersCount }) {
  const [voterAddress, setVoterAddress] = useState('');
  const methods = contract.methods;

  function handleVoterChange(event: any) {
    setVoterAddress(event.target.value);
  }

  function handleSubmit(event: any) {
    event.preventDefault();
    methods
      .voters(voterAddress)
      .call()
      .then((voter: any) => {
        if (!voter.isRegistered) {
          setVotersCount(votersCount + 1);
          methods.registerVoter(voterAddress).send({ from: userAdress });
          setVoterAddress('');
        }
      });
  }

  return (
    <div>
      <h3>Ajouter un voteur</h3>
      <form onSubmit={handleSubmit}>
        <input className='form-control' type='text' value={voterAddress} onChange={handleVoterChange}></input>
        <button className='btn btn-primary mt-2'>Ajouter</button>
      </form>
    </div>
  );
}
