import 'bootstrap/dist/css/bootstrap.css'
import '../../public/css/main.css'

export const metadata = {
  title: 'Dapp Zaidi Mehdi',
  description: 'Projet crypto IUT',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="fr">
      <body>{children}</body>
    </html>
  )
}
