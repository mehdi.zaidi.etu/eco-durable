'use client';
import { useState, useEffect } from 'react';
import Web3 from 'web3';
import { validator } from 'web3-validator';
import config from '../../build/contracts/Voting.json';
import ProposalForm from './components/ProposalForm';
import VoterForm from './components/VoterForm';
import ChangeWorkflow from './components/buttons/ChangeWorkflow';
import ProposalCard from './components/ProposalCard';

const web3 = new Web3('http://127.0.0.1:7545');
const CONTRACT_ADDRESS = config.networks[5777].address;
const CONTRACT_ABI = config.abi;

const contract = new web3.eth.Contract(CONTRACT_ABI, CONTRACT_ADDRESS);
const methods = contract.methods;

export default function Home() {
  const [currentAccount, setCurrentAccount] = useState('loading');
  let [proposalsCount, setProposalsCount] = useState(0);
  let [votersCount, setVotersCount] = useState(0);
  let [canRegister, setCanRegister] = useState(false);
  let [canVote, setCanVote] = useState(false);
  let [canProposal, setCanProposal] = useState(false);
  let [isOwner, setIsOwner] = useState(false);
  let [cards, setCards] = useState<any[]>([]);

  web3.eth.getAccounts().then(accounts => {
    const accountAddr = accounts[0];
    validator.validate(['address'], [accountAddr]);
    setCurrentAccount(accountAddr);

    methods
      .canRegister()
      .call()
      .then((enabled: any) => {
        setCanRegister(enabled);
        if (enabled) {
          methods
            .voters(accountAddr)
            .call()
            .then((voter: any) => {
              console.log(voter);
              if (!voter.isRegistered) {
                setVotersCount(votersCount++);
                methods.registerVoter(accountAddr).send({ from: accountAddr });
              }
            });
        }
      });

    methods
      .owner()
      .call()
      .then((owner: any) => {
        if (owner == accountAddr) {
          setIsOwner(true);
        }
      });

    methods
      .canProposal()
      .call()
      .then((enabled: any) => {
        setCanProposal(enabled);
      });

    methods
      .canVote()
      .call()
      .then((enabled: any) => {
        setCanVote(enabled);
      });

    methods
      .getProposalsCount()
      .call()
      .then((count: any) => {
        setProposalsCount(parseInt(count));

        // for (let index = 0; index < parseInt(count); index++) {
        //   methods
        //     .proposals(index)
        //     .call()
        //     .then((proposal: any) => {
        //       proposal.index = index;
        //       setCards(cards => [...cards, proposal]);
        //     });
        // }
      });

    methods
      .getVotersCount()
      .call()
      .then((count: any) => setVotersCount(parseInt(count)));
  });

  return (
    <main className='container mt-5'>
      <h2>Votre compte: {currentAccount}</h2>
      <h2>Admin: {isOwner ? 'oui' : 'non'}</h2>
      <h2>Nombres de propositions: {proposalsCount}</h2>
      <h2>Nombres d'inscrits: {votersCount}</h2>
      <ul>
        <li>Vote: {canVote ? 'oui' : 'non'}</li>
        <li>Register: {canRegister ? 'oui' : 'non'}</li>
        <li>Proposal: {canProposal ? 'oui' : 'non'}</li>
      </ul>
      {isOwner && canRegister && <VoterForm contract={contract} userAdress={currentAccount} setVotersCount={setVotersCount} votersCount={votersCount}></VoterForm>}
      {canProposal && <ProposalForm proposalCount={proposalsCount} setProposalCount={setProposalsCount} contract={contract} userAddress={currentAccount}></ProposalForm>}
      {cards.map((element: any) => (
        <ProposalCard contract={contract} userAddress={currentAccount} text={element.description} number={element.index}></ProposalCard>
      ))}
      {isOwner && canRegister && <ChangeWorkflow oldState={setCanRegister} newState={setCanProposal} userAdress={currentAccount} contract={contract} text={"Commencer l'ajout de propositions"} state={'startProposalsRegistration'}></ChangeWorkflow>}
      {/* {isOwner && canProposal && <ChangeWorkflow oldState={setCanProposal} newState={setCanVote} userAdress={currentAccount} contract={contract} text={'Commencer la phase de vote'} state={'startVotingSession'}></ChangeWorkflow>} */}
      {isOwner && canVote && <ChangeWorkflow oldState={setCanVote} newState={setCanRegister} userAdress={currentAccount} contract={contract} text={'Arrêter les votes'} state={'endVotingSession'}></ChangeWorkflow>}
    </main>
  );
}
